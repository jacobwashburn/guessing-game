from django.db import models

# Create your models here.


class Person(models.Model):
    name = models.CharField(max_length=50)
    age = models.PositiveIntegerField()
    # movie = models.ManyToManyField(Movie)

class Movie(models.Model):
    name = models.CharField(max_length=50, unique=True)
    release_date = models.DateField(blank=True, null=True)
    # user = models.ManyToManyField(Person)
