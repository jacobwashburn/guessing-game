import random
b = 1
c = 9
d = "Please enter an integer from " + str(b) + " to " + str(c) + " or type exit to quit: "
e = 1
g = "exit"


def get_random(min, max):
    r = random.randint(min, max)
    return r


def get_guess(d):
    g = raw_input(d)
    if g.isalpha() and g.lower() != "exit":
        get_guess("That was a bad guess " + str(g) + " " + d)

    if g.lower() == "exit":
        print "Goodbye"
        exit()
    elif g.isdigit():
        print
        s = int(g)

    return s


a = get_random(b, c)

guess = get_guess(d)

while a != guess:
    e = e + 1
    print
    if guess < a:
        print str(guess) + " Is to low"
        print
        guess = get_guess(d)
    else:
        print str(guess) + " Is to high"
        print
        guess = get_guess(d)
print
print "You guessed it! The Number was " + str(guess) + "!"
print "It took you " + str(e) + " guesses!"


